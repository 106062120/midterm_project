# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : softwarre studio mid project
* Key functions (add/delete)
    1. chat
    2. load message history
    3. chat with new user
    
* Other functions (add/delete)
    1. username
    2. message time
    3. click an user then chat
    4. sign up then sign in

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://software-studio-mid-proj-8975e.firebaseapp.com/

# Components Description : 
1. Membership Mechanism : you can create an account then log in
2. Firebase Page : deploy to firebase to host
3. Database : create an userslist data and message data for each chatroom
4. RWD : fit on different size of window, using bootstrap css
5. Topic Key Function : chat with user by choose the user from userlist, chat room show the mesesage history, new user will synchronously show on userlist
6. Third-Party Sign In : google login is avaliable
7. Chrome Notification : when a new user sign up for this web, it will give a notification

# Other Functions Description(1~10%) : 
1. Username : use a database to save all username, user can be recognize by username. The top-left corner will show current user's username
2. message time : show message time
3. click and user then chat : load userlist and add an userlist to html, use 'Get' to pass value to other html file, and chat with an user be choosed 
4. sign up the sign in : after you create the account, you just have login,  don't need to log in by yourself.

## Security Report (Optional)
1. replace the special characters to its entitle, like < to &lt
2. if user not login, the html will relocation to sign in page, can't do anything if you are not login