var strUrl = location.search;
var ParaVal;
var chatid = '';

if (strUrl.indexOf("?") != -1) {
	var getSearch = strUrl.split("?");
	ParaVal = getSearch[1].split("=");
	chatid=ParaVal[1];
}

function init(){
	var back_btn = document.getElementById('back_btn');
	back_btn.addEventListener('click', function(){
		window.location.assign("main.html");
	});
    return new Promise((resolve) => {
        var user_email = '';
        var userId = '';
        var cur_user = '';
        firebase.auth().onAuthStateChanged(function (user) {
            var menu = document.getElementById('dynamic-menu');
            // Check user login
            if (user) {
                user_email = user.email;
                userId = user.uid;
                menu.innerHTML = "<span class='dropdown-item' id='logout-btn'>Logout</span>";
                /// TODO 5: Complete logout button event
                ///         1. Add a listener to logout button 
                ///         2. Show alert when logout success or error (use "then & catch" syntex)
                var btnLogout= document.getElementById('logout-btn');
                btnLogout.addEventListener('click', function () {
                    firebase.auth().signOut()
                    .then(function(messege){

                    })
                    .catch(function(error){
    
                    });
                });
                //check username
                firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
                    cur_user = snapshot.val().username;
                    var uname = document.getElementById('user_name');
                    uname.innerHTML = 'Welcome, '+cur_user;
                    resolve(cur_user);
                }).catch(function(){
                    window.location.assign("username.html");
                });
            } else {
                window.location.assign("index.html");
            }
        });
	});
}

function chatroom(cur_user){
	//data name
	var username = new Array(2);
	username[0] = cur_user;
	username[1] = chatid;
	username.sort();

	if(chatid == 'all_user'){
		var dataname = 'message/all_user';
	}else{
		var dataname = 'message/'+username[0]+'_'+username[1];
	}

	//show
	var total_chat=[];
	var l_str_before_message="<div class='mt-2 ml-2 row'><div class='bg-white py-1 px-3 mx-3' style='width: fit-content; border-radius: 15px;'><h4 style='word-wrap: break-word; max-width: 250px'>";
	var l_str_after_message="</h4></div></div><small class='ml-4'>";
	var l_str_after_time="</small>";
	var a_str_before_user="<small class='ml-4'>"
	var a_str_before_message=":</small><div class='mt-2 ml-2 row'><div class='bg-white py-1 px-3 mx-3' style='width: fit-content; border-radius: 15px;'><h4 style='word-wrap: break-word; max-width: 250px'>";
	var a_str_after_message="</h4></div></div><div><small class='ml-4'>";
	var a_str_after_time="</small></div>";
	var r_str_before_message="<div class='row justify-content-end mt-2 mr-3'><div class='bg-white py-1 px-3' style='width: fit-content; border-radius: 15px;'><h4 style='word-wrap: break-word; max-width: 250px'>";
	var r_str_after_message="</h4></div></div><div class='row justify-content-end mr-4'><small>";
	var r_str_after_time="</small></div>";
	var chatRef = firebase.database().ref(dataname);
	chatRef.on('value', function(snapshot){
		total_chat=[];
		snapshot.forEach(function (childSnapshot) {
			var value = childSnapshot.val();
			if(value.user == cur_user){
				total_chat.push(r_str_before_message+value.message+r_str_after_message+value.time+r_str_after_time);
			}else{
				if(chatid == 'all_user'){
					total_chat.push(a_str_before_user+value.user+a_str_before_message+value.message+a_str_after_message+value.time+a_str_after_time);
				}else{
					total_chat.push(l_str_before_message+value.message+l_str_after_message+value.time+l_str_after_time);
				}
			}
		});
		document.getElementById('chat_list').innerHTML = total_chat.join(' ');
		updateScroll();
	});

	//send
	send_btn = document.getElementById('send_btn');
	message_txt = document.getElementById('message');
	send_btn.addEventListener('click', function(){
		//date
		var dt= ''+new Date();
		var time = dt.split(" ")
		if(message_txt.value != ""){
			var message = message_txt.value;
			message = message.replace(/&/g, '&amp');
			message = message.replace(/</g, '&lt');
			firebase.database().ref(dataname).push({
				user: cur_user,
				message: message,
				time: time[1]+' '+time[2]+' '+time[4]
            });
		}
		message_txt.value = "";
	});
}

function updateScroll(){
    var element = document.getElementById("chat_list");
    element.scrollTop = element.scrollHeight;
}

window.onload = function (){
	document.getElementById('chatid').innerHTML = chatid;
    init()
    .then(cur_user => chatroom(cur_user));
};