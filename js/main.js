// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe(new_user) {
      console.log(Notification.permission);
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification('Notification title', {
        icon: 'img/icon.png',
        body: "Hey! There's a new user '"+new_user+"'!",
      });
  
      notification.onclick = function () {
        window.open("https://106062120.gitlab.io/midterm_project");      
      };
  
    }
  
}

function init(){
    return new Promise((resolve) => {
        var user_email = '';
        var userId = '';
        var cur_user = '';
        firebase.auth().onAuthStateChanged(function (user) {
            var menu = document.getElementById('dynamic-menu');
            // Check user login
            if (user) {
                user_email = user.email;
                userId = user.uid;
                menu.innerHTML = "<span class='dropdown-item' id='logout-btn'>Logout</span>";
                /// TODO 5: Complete logout button event
                ///         1. Add a listener to logout button 
                ///         2. Show alert when logout success or error (use "then & catch" syntex)
                var btnLogout= document.getElementById('logout-btn');
                btnLogout.addEventListener('click', function () {
                    firebase.auth().signOut()
                    .then(function(messege){

                    })
                    .catch(function(error){
    
                    });
                });
                //check username
                firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
                    cur_user = snapshot.val().username;
                    var uname = document.getElementById('user_name');
                    uname.innerHTML = 'Welcome, '+cur_user;
                    resolve(cur_user);
                });
            } else {
                window.location.assign("index.html");
            }
        });
    });
}
function userlist(cur_user){
    //the html code of user
    var str_before_username = "<div class='p-3 my-4 mx-5 bg-white rounded box-shadow userlist' id='";
    var str_mid_username =   "'><div class='media'><img class='align-self-end mr-3 rounded' src='img/user.png' alt='' style='height: 35px; width: 35px'><div class='media-body'><h5 class='mt-0'>";
    var str_after_username = "</h5></div></div></div>\n";
    var total_post = [];
    var total_user = [];
    var not_flag = false;

    var usernameRef = firebase.database().ref('username');
    usernameRef.on('value', function (snapshot) {
        /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
        ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
        ///         2. Join all post in list to html in once
        ///         4. Add listener for update the new post
        ///         5. Push new post's html to a list
        ///         6. Re-join all post in list to html when update
        ///
        ///         Hint: When history post count is less then new post count, update the new and refresh html
        total_post = [];
        snapshot.forEach(function (childSnapshot) {
            var value = childSnapshot.val();
            if(cur_user!=value.username){
            total_post.push(str_before_username + value.username + str_mid_username + value.username + str_after_username);
            total_user.push(value.username);
            }
        });
        if(not_flag){
            notifyMe(total_user[total_user.length-1]);
        }else{
            not_flag=true;
        }
        document.getElementById('user_list').innerHTML = total_post.join(' ');

        total_user.forEach(function(childuser) {
            choose_user = document.getElementById(childuser);
            choose_user.addEventListener('click', function(){  
                window.location.assign("chatroom.html?id=" + encodeURIComponent(childuser));
            });
        });
    });
    //all
    all_user = document.getElementById('all_user');
    all_user.addEventListener('click', function(){
        window.location.assign("chatroom.html?id=all_user");
    });
}

window.onload = function (){
    init()
    .then(cur_user => userlist(cur_user));
};

