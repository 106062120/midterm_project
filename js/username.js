function init() {
    var username_btn = document.getElementById('btnUsername');
    var txtUsername = document.getElementById('username');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');

    username_btn.addEventListener('click', function(){
        if(txtUsername!=""){
            firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
            .then(function(){
                setTimeout(() => {
                    create_alert("success", "");
                    firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
                    .then(function(){
                        log_in(txtEmail.value, txtUsername.value);
                    })
                    .catch(function(error) {
                    // Handle Errors here.
                    create_alert("error", error);
                    });
                }, 1000); 
            })
            .catch(function(error) {
                // Handle Errors here.
                create_alert("error", error);
                txtEmail.value = "";
                txtPassword.value = "";
                txtUsername.value = "";
            });
        }
    });
}

function log_in(mail, name){
    var userId = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            user_email = user.email;
            userId = user.uid;

            firebase.database().ref('username').push({
                username: name
            })
            firebase.database().ref('users/'+userId).set({
                email: mail,
                username: name
            })
            setTimeout(() => {
                window.location.assign('main.html');
            }, 1000);
        } else {
            create_alert("error", 'Please sign up again');
        }
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    init();
};